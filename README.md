## Startup Clicker

An idle clicker built with React/Redux.

Play the game at [https://chriskingwebdev.gitlab.io/StartupClicker](https://chriskingwebdev.gitlab.io/StartupClicker).

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Run with `npm start`
