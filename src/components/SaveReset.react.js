import React, { Component, PropTypes } from "react";

class SaveReset extends Component {
    confirmReset() {
        if (confirm("Reset progress?")) {
            this.props.resetState();
        }
    }
    render() {
        return (
            <div id="saveResetPanel">
                <button onClick={this.props.saveState} >Save</button>
                <button onClick={this.confirmReset.bind(this)} >Reset</button>
            </div>
        );
    }
}

SaveReset.propTypes = {
    saveState: PropTypes.func,
    resetState: PropTypes.func
};

export default SaveReset;
