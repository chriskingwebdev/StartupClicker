import React, { Component, PropTypes } from "react";

class CEOActions extends Component {
    clickCeoCode(e) {
        e.preventDefault();
        if (this.props.tech >= this.props.serverSpace) {
            this.props.addErrorMessage("Insufficient Server Space");
        } else {
            this.props.ceoCode();
        }
    }
    clickCeoDesign(e) {
        e.preventDefault();
        if (this.props.design >= this.props.maxDesign) {
            this.props.addErrorMessage("Insufficient Tech for Design");
        } else {
            this.props.ceoDesign();
        }
    }
    clickCeoMarketing(e) {
        e.preventDefault();
        if (this.props.buzz >= this.props.maxBuzz) {
            this.props.addErrorMessage("Insufficient Design for Marketing");
        } else {
            this.props.ceoMarketing();
        }
    }
    clickCeoInvite(e) {
        e.preventDefault();
        this.props.ceoInvite();
    }
    clickCeoInspire(e) {
        e.preventDefault();
        if (!this.props.currentStaff) {
            this.props.addErrorMessage("No One To Inspire");
        } else {
            this.props.ceoInspire();
        }
    }
    render() {
        return (
            <div className="ceo-actions">
                <h2>CEO Actions</h2>
                <button className={`fade${this.props.ceoCodeEnabled ? "" : " disabled"}`} disabled={!this.props.ceoCodeEnabled} onClick={this.clickCeoCode.bind(this)}>
                    Code
                    <span className={`bg${this.props.ceoCodeEnabled ? "" : " disabled"}${this.props.ceoCodeAnimate ? " animate" : ""}`} style={{transitionDuration:`${this.props.ceoCodeDelay}ms`}}></span>
                </button>
                { this.renderDesignButton() }
                { this.renderMarketingButton() }
                { this.renderInviteButton() }
                { this.renderInspireButton() }
            </div>
        );
    }
    renderDesignButton() {
        if (this.props.launchLevel.level > 0) {
            return (
                <button className={`fade${this.props.ceoDesignEnabled ? "" : " disabled"}`}
                    disabled={!this.props.ceoDesignEnabled}
                    onClick={this.clickCeoDesign.bind(this)}>
                    Design
                    <span className={`bg${this.props.ceoDesignEnabled ? "" : " disabled"}${this.props.ceoDesignAnimate ? " animate" : ""}`} style={{transitionDuration:`${this.props.ceoDesignDelay}ms`}}></span>
                </button>
            );
        }
    }
    renderMarketingButton() {
        if (this.props.launchLevel.level > 1) {
            return (
                <button className={`fade${this.props.ceoMarketingEnabled ? "" : " disabled"}`}
                    disabled={!this.props.ceoMarketingEnabled}
                    onClick={this.clickCeoMarketing.bind(this)}>
                    Marketing
                    <span className={`bg${this.props.ceoMarketingEnabled ? "" : " disabled"}${this.props.ceoMarketingAnimate ? " animate" : ""}`} style={{transitionDuration:`${this.props.ceoMarketingDelay}ms`}}></span>
                </button>
            );
        }
    }
    renderInviteButton() {
        if (this.props.launchLevel.level > 0) {
            return (
                <button className={`fade${this.props.ceoInviteEnabled ? "" : " disabled"}`} disabled={!this.props.ceoInviteEnabled} onClick={this.clickCeoInvite.bind(this)}>
                    Invite
                    <span className={`bg${this.props.ceoInviteEnabled ? "" : " disabled"}${this.props.ceoInviteAnimate ? " animate" : ""}`} style={{transitionDuration:`${this.props.ceoInviteDelay}ms`}}></span>
                </button>
            );
        }
    }
    renderInspireButton() {
        return (
            <button className={`fade${this.props.ceoInspireEnabled && this.props.currentStaff ? "" : " disabled"}`} disabled={!this.props.ceoInspireEnabled && this.props.currentStaff} onClick={this.clickCeoInspire.bind(this)}>
                Inspire
                <span className={`bg${this.props.ceoInspireEnabled && this.props.currentStaff ? "" : " disabled"}${this.props.ceoInspireAnimate ? " animate" : ""}`} style={{transitionDuration:`${this.props.ceoInspireDelay}ms`}}></span>
            </button>
        );
    }
}

CEOActions.propTypes = {
    currentStaff: PropTypes.number,
    ceoInspireEnabled: PropTypes.bool,
    ceoInspireAnimate: PropTypes.bool,
    ceoInspireDelay: PropTypes.number,
    ceoCodeEnabled: PropTypes.bool,
    ceoCodeAnimate: PropTypes.bool,
    ceoCodeDelay: PropTypes.number,
    ceoCodeLastClick: PropTypes.number,
    ceoDesignEnabled: PropTypes.bool,
    ceoDesignAnimate: PropTypes.bool,
    ceoDesignDelay: PropTypes.number,
    ceoMarketingEnabled: PropTypes.bool,
    ceoMarketingAnimate: PropTypes.bool,
    ceoMarketingDelay: PropTypes.number,
    ceoInviteEnabled: PropTypes.bool,
    ceoInviteAnimate: PropTypes.bool,
    ceoInviteDelay: PropTypes.number,
    tech: PropTypes.number,
    serverSpace: PropTypes.number,
    design: PropTypes.number,
    maxDesign: PropTypes.number,
    buzz: PropTypes.number,
    maxBuzz: PropTypes.number,
    launchLevel: PropTypes.object,
    ceoInspire: PropTypes.func.isRequired,
    ceoCode: PropTypes.func.isRequired,
    ceoDesign: PropTypes.func.isRequired,
    ceoMarketing: PropTypes.func.isRequired,
    ceoInvite: PropTypes.func.isRequired,
    addErrorMessage: PropTypes.func.isRequired
};

export default CEOActions;
