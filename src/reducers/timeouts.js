import { ceoInspireAnimate, ceoInspireEnable, ceoCodeEnable, ceoCodeAnimate, ceoDesignEnable, ceoDesignAnimate, ceoMarketingEnable, ceoMarketingAnimate, ceoInviteEnable, ceoInviteAnimate } from "../actions";
import store from "../index.js";

const timeouts = (state, action) => {
    switch (action.type) {
        case "CEO_INSPIRE":
            setTimeout(() => {
                store.dispatch(ceoInspireAnimate());
            }, 1);
            setTimeout(() => {
                store.dispatch(ceoInspireEnable());
            }, state.ceoInspireDelay);
            return Object.assign({}, state, {ceoInspireEnabled: false});
        case "CEO_INSPIRE_ANIMATE":
            return Object.assign({}, state, {ceoInspireAnimate: true});
        case "CEO_INSPIRE_ENABLE":
            return Object.assign({}, state, {ceoInspireAnimate: false, ceoInspireEnabled: true});
        case "CEO_CODE":
            setTimeout(() => {
                store.dispatch(ceoCodeAnimate());
            }, 1);
            setTimeout(() => {
                store.dispatch(ceoCodeEnable());
            }, state.ceoCodeDelay);
            return state;
        case "CEO_CODE_ANIMATE":
            return Object.assign({}, state, {ceoCodeAnimate: true});
        case "CEO_CODE_ENABLE":
            return Object.assign({}, state, {ceoCodeAnimate: false, ceoCodeEnabled: true});
        case "CEO_DESIGN":
            setTimeout(() => {
                store.dispatch(ceoDesignAnimate());
            }, 1);
            setTimeout(() => {
                store.dispatch(ceoDesignEnable());
            }, state.ceoDesignDelay);
            return state;
        case "CEO_DESIGN_ANIMATE":
            return Object.assign({}, state, {ceoDesignAnimate: true});
        case "CEO_DESIGN_ENABLE":
            return Object.assign({}, state, {ceoDesignEnabled: true, ceoDesignAnimate: false});
        case "CEO_INVITE":
            setTimeout(() => {
                store.dispatch(ceoInviteAnimate());
            }, 1);
            setTimeout(() => {
                store.dispatch(ceoInviteEnable());
            }, state.ceoInviteDelay);
            return state;
        case "CEO_INVITE_ANIMATE":
            return Object.assign({}, state, {ceoInviteAnimate: true});
        case "CEO_INVITE_ENABLE":
            return Object.assign({}, state, {ceoInviteEnabled: true, ceoInviteAnimate: false});
        case "CEO_MARKETING":
            setTimeout(() => {
                store.dispatch(ceoMarketingAnimate());
            }, 1);
            setTimeout(() => {
                store.dispatch(ceoMarketingEnable());
            }, state.ceoMarketingDelay);
            return state;
        case "CEO_MARKETING_ANIMATE":
            return Object.assign({}, state, {ceoMarketingAnimate: true});
        case "CEO_MARKETING_ENABLE":
            return Object.assign({}, state, {ceoMarketingEnabled: true, ceoMarketingAnimate: false});
        default:
            return state;
    }
};

export default timeouts;
