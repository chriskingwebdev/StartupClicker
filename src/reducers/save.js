import { addErrorMessage } from "../actions";
import store from "../index.js";
import * as base64 from "base-64";
import { defaultState, addFunctionsToState } from "../utils/defaultState";

const save = (state, action) => {
    switch (action.type) {
        case "SAVE_STATE":
            let encodedData = base64.encode(JSON.stringify(state));
            localStorage.setItem("gamestate", encodedData);
            setTimeout(() => {
                store.dispatch(addErrorMessage("Game Saved!"));
            }, 1);
            return state;
        case "RESET_STATE":
            localStorage.clear("gamestate");
            state = defaultState;
            state = addFunctionsToState(state);
            return state;
        default:
            return state;
    }
};

export default save;
