//import { combineReducers } from "redux";
import * as base64 from "base-64";
import tech from "./tech";
import design from "./design";
import buzz from "./buzz";
import users from "./users";
import money from "./money";
import office from "./office";
import timeouts from "./timeouts";
import messages from "./messages";
import launch from "./launch";
import save from "./save";
import {defaultState, addFunctionsToState} from "../utils/defaultState";

let initialState = {};

initialState = addFunctionsToState(initialState);

let storedState = localStorage.getItem("gamestate");

if (storedState) {
    let decodedState = JSON.parse(base64.decode(storedState));
    initialState = Object.assign({}, decodedState, initialState, {
        messages: [],
        ceoInspireEnabled: true,
        ceoInspireAnimate: false,
        ceoCodeEnabled: true,
        ceoCodeAnimate: false,
        ceoDesignEnabled: true,
        ceoDesignAnimate: false,
        ceoMarketingEnabled: true,
        ceoMarketingAnimate: false,
        ceoInviteEnabled: true,
        ceoInviteAnimate: false
    });
} else {
    initialState = Object.assign({}, defaultState, initialState);
}

//These are for testing
// defaultState.ceoTechPerCode = 1000;
// defaultState.tech = 2000;
// defaultState.ceoCodeDelay = 40;
// defaultState.ceoDesignDelay = 40;
// defaultState.ceoInviteDelay = 40;
// defaultState.servers = 5;
// defaultState.techPerServer = 10000;
// defaultState.ceoDesignPerClick = 1000;
// defaultState.ceoBuzzPerClick = 1000;
// defaultState.ceoMarketingDelay = 40;
// defaultState.moneyPerUser = 2;

const startUpApp = (state = initialState, action) => {
    state = tech(state, action);
    state = design(state, action);
    state = buzz(state, action);
    state = users(state, action);
    state = money(state, action);
    state = office(state, action);
    state = launch(state, action);
    state = timeouts(state, action);
    state = messages(state, action);
    state = save(state, action);
    return state;
};

export default startUpApp;
