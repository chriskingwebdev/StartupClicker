export const defaultState = {
    ceoInspireEnabled: true,
    ceoInspireAnimate: false,
    ceoInspireDelay: 10000,

    tech:0,
    ceoCodeEnabled: true,
    ceoCodeAnimate: false,
    ceoTechPerCode: 1,
    ceoCodeDelay: 200,

    coders: 0,
    costPerCoder: 20,
    coderEfficency: 1,

    servers: 1,
    techPerServer: 100,
    serverCost: 100,
    serverUpgradeCost: 500,

    design: 0,
    ceoDesignEnabled: true,
    ceoDesignAnimate: false,
    ceoDesignPerClick: 1,
    ceoDesignDelay: 300,

    designers: 0,
    costPerDesigner: 20,
    designerEfficency: .5,

    buzz: 0,
    ceoMarketingEnabled: true,
    ceoMarketingAnimate: false,
    ceoBuzzPerClick: 1,
    ceoMarketingDelay: 500,

    marketers: 0,
    costPerMarketer: 20,
    marketerEfficency: .25,

    ceoInviteEnabled: true,
    ceoInviteAnimate: false,
    ceoInviteDelay: 5000,
    users: [],

    allowUserInvites: false,
    userInviteSeconds: 5,
    userPaymentSeconds: 60,

    money: 0,
    moneyPerUser: 0,

    currentOffice: {name: "Living Room", space: 2},
    nextOffice: {name: "Garage", space: 5},

    launchLevel: {level: 0, status: "Not Launched"},
    nextLaunchLevel: {level: 1, status: "In Alpha", label: "Launch Alpha", tech: 500, design: 0, buzz: 0  },

    messages: []
};

export const addFunctionsToState = (state) => {
    state.serverSpace = function() {
        return this.servers * this.techPerServer;
    };
    state.maxDesign = function() {
        return this.serverSpace() * .5;
    };
    state.maxBuzz = function() {
        return this.serverSpace() * .25;
    };
    state.currentStaff = function() {
        return this.coders +  this.designers + this.marketers;
    };
    state.vacantOfficeSpace = function() {
        return this.currentOffice.space - this.currentStaff();
    };
    return state;
};
