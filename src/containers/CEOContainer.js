import { connect } from "react-redux";
import { ceoInspire, ceoCode, ceoDesign, ceoMarketing, ceoInvite, addErrorMessage } from "../actions";
import CEOActions from "../components/CEOActions.react";

const mapStateToProps = (state) => ({
    ceoInspireEnabled: state.ceoInspireEnabled,
    ceoInspireAnimate: state.ceoInspireAnimate,
    ceoInspireDelay: state.ceoInspireDelay,
    ceoCodeEnabled: state.ceoCodeEnabled,
    ceoCodeAnimate: state.ceoCodeAnimate,
    ceoCodeDelay: state.ceoCodeDelay,
    ceoCodeLastClick: state.ceoCodeLastClick,
    ceoDesignEnabled: state.ceoDesignEnabled,
    ceoDesignAnimate: state.ceoDesignAnimate,
    ceoDesignDelay: state.ceoDesignDelay,
    ceoInviteEnabled: state.ceoInviteEnabled,
    ceoInviteAnimate: state.ceoInviteAnimate,
    ceoInviteDelay: state.ceoInviteDelay,
    ceoMarketingEnabled: state.ceoMarketingEnabled,
    ceoMarketingAnimate: state.ceoMarketingAnimate,
    ceoMarketingDelay: state.ceoMarketingDelay,
    ceoTechPerCode: state.ceoTechPerCode,
    tech: state.tech,
    serverSpace: state.serverSpace(),
    design: state.design,
    maxDesign: state.maxDesign(),
    buzz: state.buzz,
    maxBuzz: state.maxBuzz(),
    launchLevel: state.launchLevel,
    currentStaff: state.currentStaff()
});

const mapDispatchToProps = (dispatch) => ({
    ceoInspire: () => {
        dispatch(ceoInspire());
    },
    ceoCode: () => {
        dispatch(ceoCode());
    },
    ceoDesign: () => {
        dispatch(ceoDesign());
    },
    ceoMarketing: () => {
        dispatch(ceoMarketing());
    },
    ceoInvite: () => {
        dispatch(ceoInvite());
    },
    addErrorMessage: (message) => {
        dispatch(addErrorMessage(message));
    }
});

const CEOContainer = connect(mapStateToProps, mapDispatchToProps)(CEOActions);

export default CEOContainer;
