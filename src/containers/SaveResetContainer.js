import { connect } from "react-redux";
import { saveState, resetState } from "../actions";
import SaveReset from "../components/SaveReset.react";

const mapDispatchToProps = (dispatch) => ({
    saveState: () => {
        dispatch(saveState());
    },
    resetState: () => {
        dispatch(resetState());
    }
});

const SaveResetContainer = connect(null, mapDispatchToProps)(SaveReset);

export default SaveResetContainer;
